package MonsterApiInternal::Client;

use strict;
use warnings;
use JSON;
use Digest::SHA qw(hmac_sha256_hex); 
use LWP::UserAgent;
use HTTP::Request;

sub new {
  my $class = shift;
  my $hostandport = shift;
  my $api_secret = shift;
  my $other_params = shift || {};
  
  die "Api secret missing" if(!$api_secret);
  
  my $d = {
    "_host_and_port"=> $hostandport,
	"_api_secret"=> $api_secret,
	"_ua" => LWP::UserAgent->new,
	"_params" => $other_params,
  };

  
  return bless $d, $class;
}

sub fetch_task {
  my $c = shift;
  my $uri = shift;
  my $task_id = shift;
  my $destination_file = shift;

  if($uri !~ m#^(/[^/]+)#) {
    die "Cannot determine the URI prefix!";
  }

  my $api_prefix = $1;
  my $full_url = "http://$c->{_host_and_port}$api_prefix/tasks/".$task_id;
  print STDERR "Fetching task from: $full_url and saving as $destination_file\n";
  return $c->{_ua}->mirror($full_url, $destination_file);
}

sub request {
  my $c = shift;
  my $method = shift;
  my $uri = shift;
  my $payload = shift || "";
  
  if(($payload)&&(ref($payload) ne "")) {
    #print "ref: ".ref($payload)." $payload\n";
    $payload = to_json($payload);
  }
  
  my $now = time();
=skip
  my $sigData = $now." ".$uri." ".$payload;
  my $sig = hmac_sha256_hex($sigData, $c->{'_api_secret'});
  #print "SigData: $sigData\nSig: $sig\n";
  my $fullUri = "$uri?ts=$now&sig=$sig";
=cut
  my $fullUri = "$uri?ts=$now";
  my $fullUrl = "http://".$c->{'_host_and_port'}.$fullUri;
  my $r = HTTP::Request->new( $method => $fullUrl );
  $r->header("X-Mc-Secret"=>$c->{_api_secret});
  $r->content($payload) if($payload);

  my $re;
  eval {  
	  my $res = $c->{'_ua'}->simple_request( $r );
	  $re = from_json($res->content);
	  
	  if(!$res->is_success) {
		 die "request was not successful";
	  }

    my $resp = from_json($res->content);
    $payload = $resp->{"result"};

=skip
	  die "No timestamp in the response" if(!$re->{'ts'});
	  die "Response too old" if($re->{'ts'} < time() - 5);
	  #print $res->content;
	  
	  # the regexp based approach for result extraction is unfortunately needed 
	  # because the order of hash elements is undefined in Perl
	  
	  die "result not found" if($res->content !~ /"result":(.+),"sig":"/s);
	  $payload = $1;
	  $payload = $1 if($payload =~ /^"(.+)"$/s);
	  $sigData = $re->{'ts'}." ".$uri." ".$payload;
	  #print "sigdata: $sigData\n";
	  $sig = hmac_sha256_hex($sigData, $c->{'_api_secret'});
	  if($sig ne $re->{'sig'}){
		 die "Signature verification of the response failed" if(($ENV{"MONSTERAPI_ENV"})&&($ENV{"MONSTERAPI_ENV"} ne "development"));
	  }
=cut

	  if(($c->{"_params"}->{'die_on_error'})&&($re->{'error'})) {
		 die "Error response";
	  }
  };
  if($@) {
     die "Request failed: $@\nServer: $c->{_host_and_port}\nMethod: $method\nUri: $uri\nParams: $payload\nResponse: ".($re?to_json($re):"")."\n\n\n\n";
  }

  $re = $re->{'result'} if($c->{'_params'}->{'result_only'});
  
  return $re;
}

sub get {
  my $c = shift;
  my $uri = shift;

  return $c->request("GET", $uri, "");
}
sub head {
  my $c = shift;
  my $uri = shift;

  return $c->request("HEAD", $uri, "");
}
sub search {
  my $c = shift;
  my $uri = shift;

  return $c->request("SEARCH", $uri, "");
}
sub put {
  my $c = shift;
  my $uri = shift;
  my $payload = shift;

  return $c->request("PUT", $uri, $payload);
}

sub post {
  my $c = shift;
  my $uri = shift;
  my $payload = shift;

  return $c->request("POST", $uri, $payload);
}

sub delete {
  my $c = shift;
  my $uri = shift;
  my $payload = shift;

  return $c->request("DELETE", $uri, $payload);
}

sub ping {
  my $c = shift;
  return $c->get("/ping");
}


1;
