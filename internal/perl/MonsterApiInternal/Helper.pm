package MonsterApiInternal::Helper;

use strict;
use warnings;
use File::Slurp;
use JSON;
use MonsterApiInternal::Client;
use constant {
  CONFIG_FILES=>"/etc/monster",
};


sub get_service_config_by_service_name {
   my $class = shift;
   my $service_name = shift;

   return get_json_from_file(get_conf_file_path($service_name));

}
sub get_volume_config_by_service_config {
   my $class = shift;
   my $service_config = shift;

   return get_json_from_file(find_volume_file_in_dir($service_config->{"volume_directory"}));
}
sub get_volume_config_by_service_name {
   my $class = shift;
   my $service_name = shift;

   my $service_config = MonsterApiInternal::Helper->get_service_config_by_service_name($service_name);

   return MonsterApiInternal::Helper->get_volume_config_by_service_config($service_config);
}

sub new {
   my $class = shift;
   my $service_name = shift;

   my $service_config;
   if($service_name =~ /^{/) {
     $service_config = from_json($service_name);
   } else {
     $service_config = MonsterApiInternal::Helper->get_service_config_by_service_name($service_name);
   }
   
   my $service_volume = MonsterApiInternal::Helper->get_volume_config_by_service_config($service_config);

   return MonsterApiInternal::Client->new('127.0.0.1:'.$service_config->{'listen_port'}, $service_volume->{'api_secret'}, @_);

}


sub get_json_from_file {
  my $f = shift;
  my $c = read_file($f);
  return from_json($c);
}

sub find_volume_file_in_dir {
  my $dir = shift;
  opendir(my $x, $dir) or die "cant open: $dir: $!";
  my @files = grep {(/\.json$/) && ($_ ne "state.json") } readdir($x);
  closedir($x);

  die "Multiple/no config files in $dir" if(scalar @files != 1);
  return $dir."/".$files[0];
}

sub get_conf_file_path {
  my $n = shift;
  return CONFIG_FILES."/".$n.".json";
}


1;
