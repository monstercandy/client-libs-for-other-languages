#!/usr/bin/perl

use strict;
use warnings;
use FindBin qw($Bin);
use JSON;
BEGIN {
  push @INC, $Bin;
};
use MonsterApiInternal::Client;

die "Usage: $0 host:port api_secret method uri [payload]" if(scalar @ARGV < 4);

my $c = new MonsterApiInternal::Client(shift @ARGV, shift @ARGV);
my $r = $c->request(shift @ARGV, shift @ARGV, shift @ARGV);
$r = ref($r) ne "" ? encode_json($r) : $r;
print $r;
