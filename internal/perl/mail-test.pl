#!/usr/bin/perl

use strict;
use warnings;
use JSON;
use File::Slurp;
use FindBin qw($Bin);
use Data::Dumper;
BEGIN {
  push @INC, $Bin;
};
use MonsterApiInternal::Helper;

my $service_name = shift @ARGV;
my $to = shift @ARGV;
my $template = shift @ARGV || "account/registration";
my $locale = shift @ARGV || "en";

die "Usage: $0 mail_service_name to [template|account/registration] [locale|en]" if((!$service_name)||(!$to));


my $c = MonsterApiInternal::Helper->new($service_name);


my $x = {};
$x->{to} = $to;
$x->{locale} = $locale;
$x->{template} = $template;
$x->{context} = {"foo"=>"bar" };

my $r = $c->post("/mailer/sendmail", $x);
print Dumper($r);

