#!/usr/bin/perl

use strict;
use warnings;
use FindBin qw($Bin);
use Data::Dumper;
BEGIN {
  push @INC, $Bin;
};
use MonsterApiInternal::Helper;

my $service_to_add_into = shift @ARGV;
my $service_to_add_into_api_prefix = shift @ARGV;
my $service_name = shift @ARGV;
my $host = shift @ARGV;
my $username = shift @ARGV;
my $password = shift @ARGV;
my $api_prefix = shift @ARGV;
my $port = shift @ARGV;

die "Usage: $0 service_to_add_into service_to_add_into_api_prefix service_name host username password [service_to_add_api_prefix] [port=https] 
Example: ./add-public-service.pl relayer-anonim /api tapi monstermedia.hu email password /api
Example: ./add-public-service.pl dbms /dbms info-webhosting 127.0.0.1 username password /api 10006
Example: ./add-public-service.pl mailer-monster /mailer info-email 127.0.0.1 username password /api 10006
Example: ./add-public-service.pl dbms /dbms relayer 127.0.0.1 username password /api 10006 # this is used for file upload
Example: ./add-public-service.pl account-monster /accountapi relayer 127.0.0.1 username password /api 10006
Example: ./add-public-service.pl email /email info-accountapi 127.0.0.1 username password /api 10006
Example: ./add-public-service.pl dbms /dbms eventapi 127.0.0.1 username password /api 10006
" if(!$password);


my $c = MonsterApiInternal::Helper->new($service_to_add_into);

my $payload = {"scheme"=>$port ? "http": "https","host"=>$host,"username"=>$username,"password"=>$password};
$payload->{'port'} = $port if($port);

my $method = $ENV{PS_RESET} ? "POST" : "PUT";

$payload->{'api_prefix'} = $api_prefix if($api_prefix);
print Dumper($c->request($method, "$service_to_add_into_api_prefix/service/$service_name", $payload));

#./generic.pl 127.0.0.1:10006 7394625a73418fed91524e11426f7212 PUT /api/service/accountapi '{"host":"127.0.0.1","port":10004,"api_secret":"122bce501850954ace9711ed37e58424"}'


