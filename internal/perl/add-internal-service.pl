#!/usr/bin/perl

use strict;
use warnings;
use FindBin qw($Bin);
use Data::Dumper;
BEGIN {
  push @INC, $Bin;
};
use MonsterApiInternal::Helper;

my $service_to_add_into = shift @ARGV;
my $service_to_add_into_api_prefix = shift @ARGV;
my $service_to_add = shift @ARGV;
my $service_name = shift @ARGV;
my $api_prefix = shift @ARGV;

die "Usage: $0 service_to_add_into api_prefix service_to_add service_name [service_to_add_api_prefix]
Example: ./add-internal-service.pl relayer-anonim /api account-anonim accountapi
Example: ./add-internal-service.pl relayer-monster /api fileman fileman
Example: WRONG_API_KEY=1 perl add-internal-service.pl dbms /dbms fileman fileman
Example: ./add-internal-service.pl webhosting /webhosting ftp ftp /ftp
Example: ./add-internal-service.pl event-monster /eventapi geoip info-geoip
Example: ./add-internal-service.pl webhosting /webhosting mailer-monster mailer
" if(!$service_name);

my $conf_service_to_add = MonsterApiInternal::Helper->get_service_config_by_service_name($service_to_add);
my $vol_service_to_add = MonsterApiInternal::Helper->get_volume_config_by_service_config($conf_service_to_add);

my $c = MonsterApiInternal::Helper->new($service_to_add_into);

my $method = $ENV{PS_RESET} ? "POST" : "PUT";

my $payload = {"host"=>"127.0.0.1","port"=>$conf_service_to_add->{'listen_port'}, "api_secret"=> $ENV{'WRONG_API_KEY'} ? "x" : $vol_service_to_add->{'api_secret'}};
$payload->{'api_prefix'} = $api_prefix if($api_prefix);
print Dumper($c->request($method, "$service_to_add_into_api_prefix/service/$service_name", $payload));

#./generic.pl 127.0.0.1:10006 7394625a73418fed91524e11426f7212 PUT /api/service/accountapi '{"host":"127.0.0.1","port":10004,"api_secret":"122bce501850954ace9711ed37e58424"}'


