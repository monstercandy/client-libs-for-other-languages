#!/usr/bin/perl

use strict;
use warnings;
use FindBin qw($Bin);
use JSON;
BEGIN {
  push @INC, $Bin;
};
use MonsterApiInternal::Helper;

die "Usage: $0 service_name method uri [payload]" if(scalar @ARGV < 3);

my $service_name = shift @ARGV;
my $method = shift @ARGV;
my $uri = shift @ARGV;
my $payload = shift @ARGV;

my $c = new MonsterApiInternal::Helper($service_name);
my $r = $c->request($method, $uri, $payload);
my $rjs = ref($r) ne "" ? encode_json($r) : $r;
print $rjs;

if(($ENV{MC_TASK_DOWNLOAD})&&(ref $r->{result} eq "HASH")) {
	my $task_id = $r->{result}->{id};
	if($task_id) {
		$c->fetch_task($uri, $task_id, $ENV{MC_TASK_DOWNLOAD});
	}
}
