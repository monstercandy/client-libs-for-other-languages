<?php
namespace MonsterApiInternal;


function fetchurlngjson($url, $postdata = "", $header = "") {
  $re = fetchurlng($url, $postdata, $header);
  return json_decode($re, true);
}

function fetchurlng($url, $postdata = "", $header = "") {
  $ch = curl_init();
  // set URL and other appropriate options
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
  curl_setopt($ch, CURLOPT_TIMEOUT, 30);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
  if($header==="") $header = Array();
  if(is_string($header)) $header = Array($header);
  array_push($header, "X-Forwarded-For: ".$_SERVER["REMOTE_ADDR"]);
  if($_SERVER["HTTP_MONSTERTOKEN"])
    array_push($header, "MonsterToken: ".$_SERVER["HTTP_MONSTERTOKEN"]);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array($header));
  if($postdata != "") {
     curl_setopt($ch, CURLOPT_POST      ,1);
     curl_setopt($ch, CURLOPT_POSTFIELDS , $postdata);
  }
  // grab URL and pass it to the browser
  $kimenet = curl_exec($ch);
  $err = curl_error($ch);
  // close cURL resource, and free up system resources
  curl_close($ch);

   if((!$response)and($err)) {
     throw new Exception('Unable to get response from web service: '.$err);
   }


  return $kimenet;
}


class Client {

   function __construct($host, $port, $api_secret, $uriPrefix = "")  {
       if(!$api_secret)
       	 throw new \Exception("Api secret is required");

       $this->host = $host;
       $this->port = $port;
       $this->api_secret = $api_secret;
	   $this->uriPrefix = $uriPrefix;
	   $this->signature_verification = true;

   }

   function request($method, $uri, $payload = "")
   {
		  if(($payload)&&(!is_string($payload))) {
		    #print "ref: ".ref($payload)." $payload\n";
		    $payload = json_encode($payload);
		  }
		  
		  $uri = $this->uriPrefix.$uri;
		  
		  $now = time();
      /*
		  $sigData = $now." ".$uri." ".$payload;
		  $sig = hash_hmac('sha256', $sigData , $this->api_secret);

		  #print "SigData: $sigData\nSig: $sig\n";
		  $fullUri = "$uri?ts=$now&sig=$sig";
      */
      $fullUri = "$uri?ts=$now";
		  $fullUrl = "http://".$this->host.":".$this->port.$fullUri;

		  $ch = curl_init();

		  // set URL and other appropriate options
		  curl_setopt($ch, CURLOPT_URL, $fullUrl);
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);		  
          if($payload)
		     curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
		  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		  // grab URL and pass it to the browser
		  $result = curl_exec($ch);

		  if($errno = curl_errno($ch)) {
		     $error_message = curl_error($ch);
		     throw new \Exception("cURL error ({$errno}):\n {$error_message}\n$method $fullUri\n$payload\n$result");
		  }

		  // close cURL resource, and free up system resources
		  curl_close($ch);

		  
		  $re = json_decode($result, true);
		  
		  /*      
      // and now signature verification

          if(!$re['ts'])
             throw new \Exception("No timestamp in the response");
		  if($re['ts'] < time() - 5)
             throw new \Exception("Response too old");
		  #print $res->content;
		  
		  if(!preg_match('/"result":(.+),"sig":"/s',$result, $m))
		    throw new \Exception("result not found");
		  if($this->signature_verification) {
			  $payload = $m[1];
			  if(preg_match('/^"(.+)"$/s', $payload, $m)) 
			    $payload = $m[1]; 
			  $sigData = $re['ts']." ".$uri." ".$payload;
			  #print "sigdata: $sigData\n";
			  $sig = hash_hmac('sha256',$sigData, $this->api_secret);
			  if($sig != $re['sig'])
			    throw new \Exception("Signature verification of the response failed"); 
		  }
      */
  
		  
		  return $re;
   }

   function get($uri) {
   	  return $this->request("GET", $uri, "");
   }
   function head($uri) {
   	  return $this->request("HEAD", $uri, "");
   }
   function search($uri) {
   	  return $this->request("SEARCH", $uri, "");
   }
   function put($uri, $payload) {
   	  return $this->request("PUT", $uri, $payload);
   }
   function post($uri, $payload) {
   	  return $this->request("POST", $uri, $payload);
   }
   function delete($uri, $payload) {
   	  return $this->request("DELETE", $uri, $payload);
   }

   function ping() {
   	  return $this->get("/ping");
   }

}

?>