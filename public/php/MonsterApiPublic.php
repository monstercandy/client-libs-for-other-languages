<?php
namespace MonsterApiPublic;


class Client {

   const AUTH_URI = "/pub/account/authentication";

   function __construct($options)  {
       if(!is_array($options))   
       	 throw new \Exception("No options provided");
		 
       if(!isset($options["token_file"]))
       	 throw new \Exception("No token_file");
       if(!isset($options["host"]))
       	 throw new \Exception("No host");
       if(!isset($options["username"]))
       	 throw new \Exception("No username");
       if(!isset($options["password"]))
       	 throw new \Exception("No password");
		 
	   $this->options = $options;
	   
	   $this->uriPrefix = "/api";
   }
   private function doToken($newToken = ""){
		$fp = @fopen($this->options["token_file"], "c+");
		if(!$fp) throw new \Exception("Unable to access token file");

		if (flock($fp, LOCK_EX)) {  // acquire an exclusive lock
		    if($newToken) {
				ftruncate($fp, 0);      // truncate file
				fwrite($fp, $newToken);
				fflush($fp);            // flush output before releasing the lock
				
				$re = $newToken;
			} else {
			   $re = fread($fp, 100);
			}
			flock($fp, LOCK_UN);    // release the lock
		} else {
			throw new \Exception("Couldn't get the lock!");
		}
		fclose($fp);
		
		return $re;
   }
 
   function request($method, $uri, $payload = "")
   {
          $tries = 0;
          $token = $this->doToken();
		  
doLogin:
          $tries++;
          if((!$token) && ($uri != self::AUTH_URI)) {
		     $this->mydebug("doing login");
			 
		     $re = $this->post(self::AUTH_URI, Array("username"=>$this->options["username"],"password"=>$this->options["password"]));
			 $token = $this->doToken($re["token"]);
			 
			 $this->mydebug("successful login! token is: $token");
		  }
		  
		  $this->mydebug("using token: $token");
	  
		  if(($payload)&&(!is_string($payload))) {
		    #print "ref: ".ref($payload)." $payload\n";
		    $payload = json_encode($payload);
		  }
		  
		  
		  $fullUrl = "https://".$this->options["host"]. (isset($this->options["port"]) ? ":".$this->options["port"] : "").$this->uriPrefix.$uri;

		  $ch = curl_init();

		  if((isset($this->options["iamstupid"]))and($this->options["iamstupid"])) {
			  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		  }
		  
		  // set URL and other appropriate options
		  curl_setopt($ch, CURLOPT_URL, $fullUrl);
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);		  
          if($payload)
		     curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
		  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		  if($token)
		    curl_setopt($ch, CURLOPT_HTTPHEADER, Array("MonsterToken: $token"));
			
		  if($this->isDebug())
		    curl_setopt($ch, CURLOPT_VERBOSE, true);

		  // grab URL and pass it to the browser
		  $result = curl_exec($ch);

		  if($errno = curl_errno($ch)) {
		     $error_message = curl_error($ch);
		     throw new \Exception("cURL error ({$errno}):\n {$error_message}\n$method $fullUrl\n$payload\n$result");
		  }

		  // close cURL resource, and free up system resources
		  curl_close($ch);

		  $re = json_decode($result, true);
		  if(isset($re["result"]["error"])) {
		    $errorMessage = $re["result"]["error"]["message"];
		  
            if(($tries == 1)and(preg_match('/^(INVALID_TOKEN_OR_SESSION_EXPIRED|SESSION_EXPIRED|AUTH_FAILED)$/', $errorMessage))) 
			{
			   $token = "";
			   goto doLogin;
			}
		  
		    throw new \Exception($errorMessage);
		  }

		  return $re["result"];
   }

   function get($uri) {
   	  return $this->request("GET", $uri, "");
   }
   function head($uri) {
   	  return $this->request("HEAD", $uri, "");
   }
   function search($uri) {
   	  return $this->request("SEARCH", $uri, "");
   }
   function put($uri, $payload) {
   	  return $this->request("PUT", $uri, $payload);
   }
   function post($uri, $payload) {
   	  return $this->request("POST", $uri, $payload);
   }
   function delete($uri, $payload) {
   	  return $this->request("DELETE", $uri, $payload);
   }

   function ping() {
   	  return $this->get("/ping");
   }
   
   function isDebug(){
      return ((isset($this->options["debug"]))and($this->options["debug"]));
   }
   
   function mydebug($msg) {
      if(!$this->isDebug()) return;
	  
	  echo "$msg\n";
   }

}

?>