#!/usr/bin/perl

use strict;
use warnings;
use FindBin qw($Bin);
use JSON;
BEGIN {
  push @INC, $Bin;
};
use MonsterApiPublic::Client;

die "Usage: $0 host[:port] username password method uri [payload]" if(scalar @ARGV < 5);

my $c = new MonsterApiPublic::Client( { host=> shift @ARGV, username=> shift @ARGV, password => shift @ARGV, tokencache => $ENV{MC_TOKENCACHE} || "$Bin/generic.token" } );
my $method = shift @ARGV;
my $uri = shift @ARGV;
my $payload = shift @ARGV;

my $r = $c->request($method, $uri, $payload);
$r = ref($r) ne "" ? encode_json($r) : $r;
print $r;
