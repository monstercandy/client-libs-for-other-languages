package MonsterApiPublic::Client;

use strict;
use warnings;
use JSON;
use LWP::UserAgent;
use HTTP::Request;
use Fcntl qw(:flock SEEK_SET);
	
sub new {
  my $class = shift;
  my $params = shift;
  
  
  for my $c ("host","username","password") {
     die "$c missing" if(!$params->{$c});  
  }
  
  my $d = { %$params };
  $d->{"_ua"} = LWP::UserAgent->new;
  $d->{"api_prefix"} = "/api" if(!defined($d->{"api_prefix"}));
  $d->{"scheme"} = "https" if(!defined($d->{"scheme"}));
  
  return bless $d, $class;
}

sub _save_token {
  my $this = shift;
  my $token = shift;
  
  $this->{'_token'} = $token;

  # write from _token to file if supplied 
  eval {
      my $dontread = 0;
	  umask(0177);
      my $x = open(my $mbox, "+<", $this->{'tokencache'});
      if(!$x) {
	      $x = open($mbox, ">", $this->{'tokencache'});
	      $dontread = 1;
      }
	  die "Cant open cache file: $!" if(!$x);
	  
      lock($mbox);
	  eval {
	    my $key = $this->{'host'}." ".$this->{'username'};
		my $cnt = $dontread ? "{}" : <$mbox>;
		my $x = from_json($cnt);		
		$x->{$key} = $token;
		truncate($mbox, 0);
		seek($mbox, 0, SEEK_SET);
		print $mbox to_json($x);
	  };
	  
	  # write_file closes the file handle for us it seems
	  unlock($mbox);  
	  close($mbox);
  };
  
}
sub _read_token {
  my $this = shift;

  return if(!$this->{'tokencache'});

  # read from file if supplied, save it as _token
  my $token;
  eval {
      open(my $mbox, "<", $this->{'tokencache'}) or die "Cant open cache file: $!";
      lock($mbox);
	  eval {
		my $cnt = <$mbox>;
		my $x = from_json($cnt);
	    my $key = $this->{'host'}." ".$this->{'username'};		
		$token = $x->{$key};
	  };
	  
	  unlock($mbox);  
	  close($mbox);
  };
  
  $this->{'_token'} = $token;  
  return $token;
}


sub _acquire_token {
  my $this = shift;
  
  $this->_read_token() if(!$this->{'_token'});
  
  return $this->{'_token'} if($this->{'_token'});
  
  $this->login();
  
  die "Could not acquire token" if(!$this->{'_token'});
  
  return $this->{'_token'};
}

sub login {
  my $this = shift;
  my $r = $this->request("POST", "/pub/account/authentication", {"username"=> $this->{'username'}, "password"=> $this->{'password'}}, 0);  
  $this->_save_token($r->{'token'});
  
  return $r;
}

sub mydebug {
  my $this = shift;
  my $msg = shift;
  
  return if(!$this->{'debug'});
  
  print "$msg\n" ;
}

sub request {
  my $c = shift;
  my $method = shift;
  my $uri = shift;
  my $payload = shift || "";
  my $token_required = shift;
  $token_required = 1 if(!defined($token_required));
  
  
  if(($payload)&&(ref($payload) ne "")) {
    #print "ref: ".ref($payload)." $payload\n";
    $payload = to_json($payload);
  }

  $c->mydebug("MonsterApi public request: $method $uri $payload $token_required\n");
  
  my $already_retried = 0;
  
  my $fullUrl = $c->{'scheme'}."://".$c->{'host'}.$c->{"api_prefix"}.$uri;
  my $r = HTTP::Request->new( $method => $fullUrl );
  $r->content($payload) if($payload);

  my $re;
  eval {  
again:
      if($token_required) {
        $r->header("MonsterToken" => $c->_acquire_token());
	  }
	  
	  my $res = $c->{'_ua'}->simple_request( $r );
	  
	  # note the order of these lines here!
	  $re = {"error"=>$res->content};	  
	  $re = from_json($res->content);	  
	  my $ore = $re;
	  $re = $re->{'result'} if(($re->{'result'})&&($re->{'result'}));
	  

          if(!$res->is_success) {
             if(($token_required)&&
                (!$already_retried)&&
                ($re->{'error'})&&
                ($re->{'error'}->{'message'})&&
                ($re->{'error'}->{'message'} =~ /^(INVALID_TOKEN_OR_SESSION_EXPIRED|SESSION_EXPIRED|AUTH_FAILED)$/)) {
                   # token expired, and we have not attempted to retry login yet
                   $already_retried = 1;
                   $c->_save_token("");
                   goto again;
                 }

                 die "request was not successful";
          }
	  
	  die "No timestamp in the response" if(!$ore->{'ts'});
	  die "No result in the response" if(!$ore->{'result'});
	  #print $res->content;
	  
  };
  if($@) {
     die "Request failed: $@\nServer: $c->{host}\nMethod: $method\nUri: $uri\nParams: $payload\nResponse: ".($re?to_json($re):"")."\n\n\n\n";
  }
  
  return $re;
}

sub get {
  my $c = shift;
  my $uri = shift;

  return $c->request("GET", $uri, "");
}
sub head {
  my $c = shift;
  my $uri = shift;

  return $c->request("HEAD", $uri, "");
}
sub search {
  my $c = shift;
  my $uri = shift;

  return $c->request("SEARCH", $uri, "");
}
sub put {
  my $c = shift;
  my $uri = shift;
  my $payload = shift;

  return $c->request("PUT", $uri, $payload);
}

sub post {
  my $c = shift;
  my $uri = shift;
  my $payload = shift;

  return $c->request("POST", $uri, $payload);
}

sub delete {
  my $c = shift;
  my $uri = shift;
  my $payload = shift;

  return $c->request("DELETE", $uri, $payload);
}


    sub lock {
        my ($fh) = @_;
        flock($fh, LOCK_EX) or die "Cannot lock mailbox - $!\n";
        # and, in case someone appended while we were waiting...
        # seek($fh, 0, SEEK_END) or die "Cannot seek - $!\n";
    }
    sub unlock {
        my ($fh) = @_;
        flock($fh, LOCK_UN) or die "Cannot unlock mailbox - $!\n";
    }
	

1;
