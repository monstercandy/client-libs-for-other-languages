#!/usr/bin/perl

use strict;
use warnings;
use FindBin qw($Bin);
use JSON;
use File::Slurp;
use constant {
  CONFIG_FILES=>"/etc/monster",
};
BEGIN {
  push @INC, $Bin;
};
use MonsterApiPublic::Client;

die "Usage: $0 service_name username password method uri [payload]" if(scalar @ARGV < 5);

my $config = get_service_config_by_service_name(shift @ARGV);

my $c = new MonsterApiPublic::Client( { 
	scheme => "http",
	host=> "127.0.0.1:".$config->{'listen_port'},
	username=> shift @ARGV,
	password => shift @ARGV,
	tokencache => $ENV{MC_TOKENCACHE} || "$Bin/generic.token" 
} );
my $method = shift @ARGV;
my $uri = shift @ARGV;
my $payload = shift @ARGV;
my $r = $c->request($method, $uri, $payload);
$r = ref($r) ne "" ? encode_json($r) : $r;
print $r;



sub get_service_config_by_service_name {
   my $service_name = shift;

   return get_json_from_file(get_conf_file_path($service_name));

}

sub get_json_from_file {
  my $f = shift;
  my $c = read_file($f);
  return from_json($c);
}

sub get_conf_file_path {
  my $n = shift;
  return CONFIG_FILES."/".$n.".json";
}
